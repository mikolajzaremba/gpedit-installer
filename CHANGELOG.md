# Changelog

### 1.0 (2017)
Never published

### 1.1 (2017)
First Published version

### 1.2 (2017)
Major bug fixes

### 2.0 (12/19/2018)
Redesigned and simplified UI
Added EasterEgg

### 2.1 (12/21/2018)
After patching action button opens gpedit.msc
Renamed start.cmd to gpedit.cmd
Updated EasterEgg

