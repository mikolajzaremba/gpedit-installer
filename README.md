# gpedit-installer

### Gpedit.msc (Group Policy Editor) for Windows 10 Home

![Gpedit-installer Sceenshot](https://img.mikolajzaremba.ml/scrn/scrn-2019-03-22_1428.png)

Latest version: [2.1 (12/21/2018) Changelog](https://gitlab.com/mikolajzaremba/gpedit-installer/blob/master/CHANGELOG.md) <br/>
Download here: [gpedit-installer-2.1.zip](/uploads/3ae65fc3471c3fb3d9c3ec3b73438d01/gpedit-installer-2.1.zip)

### How to use? (Youtube Video Links)

English:
[![Youtube Video](http://img.youtube.com/vi/NaypJ6rwKYA/mqdefault.jpg)](http://www.youtube.com/watch?v=NaypJ6rwKYA "How to install gpedit.msc on Windows 10 Home (Easiest way)")

Polish:
[![Youtube Video](http://img.youtube.com/vi/lejC_Pi25j8/mqdefault.jpg)](http://www.youtube.com/watch?v=lejC_Pi25j8 "How to install gpedit.msc on Windows 10 Home (Jak zainstalować gpedit.msc na Windows 10 Home (Najprostszy sposób))")