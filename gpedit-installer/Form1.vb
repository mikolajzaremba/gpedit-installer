﻿Imports MaterialSkin

Public Class Form1
    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim SkinManager As MaterialSkinManager = MaterialSkinManager.Instance
        SkinManager.AddFormToManage(Me)
        SkinManager.Theme = MaterialSkinManager.Themes.LIGHT
        SkinManager.ColorScheme = New ColorScheme(Primary.BlueGrey800, Primary.BlueGrey900, Primary.BlueGrey500, Accent.LightBlue200, TextShade.WHITE)
    End Sub

    Private Sub MaterialRaisedButton1_Click(sender As Object, e As EventArgs) Handles MaterialRaisedButton1.Click
        If MaterialCheckBox2.Checked = True Then
            Dim p As Process = Process.Start(fileName:="install-console.exe")
            SkinManager.ColorScheme = New ColorScheme(Primary.Yellow800, Primary.Yellow900, Primary.Yellow500, Accent.LightBlue200, TextShade.WHITE)
            MaterialRaisedButton1.Text = "Please Wait..."
            p.WaitForExit()
            MaterialRaisedButton2.Visible = True
            MaterialRaisedButton1.Visible = False
            If MaterialCheckBox1.Checked = True Then
                Process.Start(fileName:="start.cmd")
                SkinManager.ColorScheme = New ColorScheme(Primary.Green800, Primary.Green900, Primary.Green500, Accent.LightBlue200, TextShade.WHITE)
            Else
                SkinManager.ColorScheme = New ColorScheme(Primary.Green800, Primary.Green900, Primary.Green500, Accent.LightBlue200, TextShade.WHITE)
            End If
        Else
            Dim p As Process = Process.Start(fileName:="install.exe")
            SkinManager.ColorScheme = New ColorScheme(Primary.Yellow800, Primary.Yellow900, Primary.Yellow500, Accent.LightBlue200, TextShade.WHITE)
            MaterialRaisedButton1.Text = "Please Wait..."
            p.WaitForExit()
            MaterialRaisedButton1.Text = "Done!"
            MaterialRaisedButton2.Visible = True
            MaterialRaisedButton1.Visible = False
            If MaterialCheckBox1.Checked = True Then
                Process.Start(fileName:="start.cmd")
                SkinManager.ColorScheme = New ColorScheme(Primary.Green800, Primary.Green900, Primary.Green500, Accent.LightBlue200, TextShade.WHITE)
            Else
                SkinManager.ColorScheme = New ColorScheme(Primary.Green800, Primary.Green900, Primary.Green500, Accent.LightBlue200, TextShade.WHITE)
            End If
        End If
    End Sub

    Private Sub MaterialFlatButton1_Click(sender As Object, e As EventArgs) Handles MaterialFlatButton1.Click
        about.Show()
    End Sub

    Private Sub MaterialRaisedButton2_Click(sender As Object, e As EventArgs) Handles MaterialRaisedButton2.Click
        Process.Start("gpedit.cmd")
    End Sub
End Class
